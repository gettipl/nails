/**
 * Created by Łukasz on 2015-02-01.
 */
var app = angular.module('app', ['ngRoute','textAngular']);
app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: '/pages/admin/admin.html',
                controller: 'homeController'
            })
            .when('/add', {
                templateUrl: '/pages/admin/article.html',
                controller: 'newArticleController'
            })
            .when('/edit/:id', {
                templateUrl: '/pages/admin/article.html',
                controller: 'editArticleController'
            })
            .when('/links', {
                templateUrl: '/pages/admin/links.html',
                controller: 'linksController'
            })
            .when('/gallery', {
                templateUrl: '/pages/admin/gallery.html',
                controller: 'galleryController'
            })
            .when('/entries', {
                templateUrl: '/pages/admin/entries.html',
                controller: 'entriesController'
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);