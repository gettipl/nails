app.controller('linksController', function ($scope, $http, $location) {
    $http.get('/links').success(function (data) {
        $scope.links = data;
    });
    $scope.remove = function (id) {
        $scope.links = $scope.links.filter(function (item) {
            return item._id !== id;
        });
        $http.delete('/admin/links/' + id);
    };
    $scope.add = function () {
        $scope.links.push({});
    };
    $scope.save = function () {
        $http.post('/admin/links', $scope.links.map(function (item) {
            return {_id: item._id, name: item.name, url: item.url};
        })).success(function () {
            $location.path('#');
        });
    }
});

app.controller('homeController', function ($scope) {

});

app.controller('entriesController', function ($scope, $http, $location) {
    $http.get('/article/all').success(function (data) {
        $scope.articles = data.map(function (item) {
            item.date = new Date(item.date);
            return item;
        });
    });
    $scope.remove = function (id) {
        if (confirm("Czy napewno chcesz usunąć?")) {
            $scope.articles = $scope.articles.filter(function (item) {
                return item._id !== id;
            });
            $http.delete('/admin/article/' + id);
        }
    };
    $scope.edit = function(id){
        $location.path('/edit/'+id);
    }
});

app.controller('newArticleController', function ($scope, $http, $location) {
    $scope.publish = false;
    $scope.save = function () {
        $http.post('/admin/article/add', {
            title: $scope.title,
            subtitle: $scope.subtitle,
            category: $scope.category,
            tags: $scope.tags.split(','),
            content: $scope.content,
            publish: $scope.publish,
            date: new Date()
        }).success(function () {
            $location.path('#');
        });
    }
});

app.controller('editArticleController', function ($scope, $http, $location, $routeParams) {
    $http.get('/article/' + $routeParams.id).success(function (data) {
        $scope.title = data.title;
        $scope.subtitle = data.subtitle;
        $scope.category = data.category;
        $scope.tags = data.tags.join(',');
        $scope.content = data.content;
        $scope.publish = data.publish,
        $scope.id = data._id,
        $scope.date = data.date
    });
    $scope.save = function () {
        $http.post('/admin/article', {
            title: $scope.title,
            subtitle: $scope.subtitle,
            category: $scope.category,
            tags: $scope.tags.split(','),
            content: $scope.content,
            publish: $scope.publish,
            _id : $scope.id,
            date : $scope.date
        }).success(function () {
            $location.path('');
        });
    }
});

app.controller('galleryController', function ($scope, $http) {
    $http.get('/images/').success(function (data) {
        $scope.images = data;
    });
    $scope.remove = function (id) {
        $scope.images = $scope.images.filter(function (item) {
            return item._id !== id;
        });
        $http.delete('/admin/image/' + id);
    };
});