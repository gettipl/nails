app.factory('counterService',function($http){
   return {
       update :function(){
           if(!window.sessionStorage.counterSent){
               $http.post('/counter',{});
               window.sessionStorage.counterSent = true;
           }
       }
   }
});