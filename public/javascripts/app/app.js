var app = angular.module('app',['ngRoute']);
app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: '/pages/index.html',
                controller: 'homeController'
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);