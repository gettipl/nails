app.controller('homeController', function ($scope, $http,counterService) {
    $http.get('/links').success(function (data) {
       $scope.links = data;
    });
    counterService.update();
    $http.get('/counter').success(function (data) {
       $scope.counter = data.value;
    });
});