var express = require('express');
var router = express.Router();
var links = require('./Logic/links');
var login = require('./Logic/login');
var image = require('./Logic/image');
var article = require('./Logic/article');

/* GET home page. */
router.get('/', function (req, res) {
    res.render('admin');
});

router.get('/logout', login.logout);

router.post('/links', links.post).delete('/links/:id', links.delete);

router.post('/image', image.post).delete('/image/:id', image.delete);

router.post('/article/add', article.add).delete('/article/:id',article.delete).post('/article', article.save);

module.exports = router;
