module.exports = {
    get: function(req,res){
        req.db.counter.find({},function(err, data){
            res.send(data[0]);
            res.end();
        });
    },
    post : function(req,res) {
        req.db.counter.update({}, {$inc: {value: 1}});
        res.end();
    }
};
