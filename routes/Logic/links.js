var mongojs = require('mongojs');

module.exports = {
    get: function (req, res) {
        req.db.links.find(function (err, docs) {
            res.send(docs);
            res.end();
        });
    },
    post : function (req, res) {
        req.body.forEach(function (item) {
            if (item._id)
                item._id = mongojs.ObjectId(item._id);
            req.db.links.save(item);
        });
        res.end();
    },
    delete: function(req,res){
        req.db.links.remove({_id :mongojs.ObjectId(req.params.id)});
        res.end();
    }
};
