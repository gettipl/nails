var mongojs = require('mongojs');

module.exports = {
    getOne: function(req,res){
        var id = req.params.id;
        req.db.images.findOne({_id : mongojs.ObjectId(id)},function(err, data){
            res.setHeader('content-type', data.mimetype);
            res.end(new Buffer(data.file),'binary');
        });
    },
    get:function(req,res){
        req.db.images.find({},{file:0},function(err, data){
            res.send(data);
            res.end();
        });
    },
    post: function(req,res){
        req.pipe(req.busboy);
        req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {

            var buffers = [];
            file.on('data', function(chunk) {
                console.log('got %d bytes of data', chunk.length);
                buffers.push(chunk);
            });
            file.on('end', function() {
                console.log('there will be no more data.');
                req.db.images.insert({file: Buffer.concat(buffers).toJSON(), mimetype:mimetype});
                res.redirect('/admin#/gallery');
            });
        });
    },
    delete: function(req,res){
        req.db.images.remove({_id :mongojs.ObjectId(req.params.id)});
        res.end();
    }
};
