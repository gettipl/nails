var passport = require('passport');
module.exports = {
    get: function (req, res) {
        res.render('login');
    },
    post: function (req, res, next) {
        passport.authenticate('local', function (err, user, info) {
            if (err) {
                return next(err)
            }
            if (!user) {
                req.session.messages = [info.message];
                return res.redirect('/login')
            }
            req.logIn(user, function (err) {
                if (err) {
                    return next(err);
                }
                return res.redirect('/admin');
            });
        })(req, res, next);
    },
    logout: function (req, res) {
        req.logout();
        res.redirect('/');
    }
}
