var mongojs = require('mongojs');

module.exports = {
    add: function (req, res) {
        req.db.articles.insert(req.body);
        res.end();
    },
    getAllWithoutContent: function (req, res) {
        req.db.articles.find({}, {content: 0}, function (err, data) {
            res.send(data);
            res.end();
        })
    },
    delete: function (req, res) {
        req.db.articles.remove({_id: mongojs.ObjectId(req.params.id)});
        res.end();
    },
    get: function (req, res) {
        req.db.articles.findOne({_id: mongojs.ObjectId(req.params.id)}, function (err, data) {
            res.send(data);
            res.end();
        })
    },
    save: function (req, res) {
        req.body._id = mongojs.ObjectId(req.body._id);
        req.db.articles.save(req.body);

        res.end();
    }
};
