var express = require('express');
var router = express.Router();

var links = require('./Logic/links');
var login = require('./Logic/login');
var counter = require('./Logic/counter');
var image = require('./Logic/image');
var article = require('./Logic/article');


/* GET home page. */
router.get('/', function(req, res) {
  res.render('index');
});

router.get('/login',login.get).post('/login',login.post);
router.get('/links',links.get);
router.get('/counter',counter.get).post('/counter',counter.post);
router.get('/image/:id',image.getOne);
router.get('/images',image.get);
router.get('/article/all',article.getAllWithoutContent).get('/article/:id',article.get);

module.exports = router;
